# PROJECT HAS BEEN MOVED
The project has been moved to https://gitlab.ics.muni.cz/cryton/cryton. For more information check the [documentation](https://cryton.gitlab-pages.ics.muni.cz/).

# Playground for the Cryton toolset
This project serves as a demonstration for the Cryton toolset. It allows you to try out the Cryton toolset in action and run E2E tests.

Cryton toolset is tested and targeted primarily on **Debian** and **Kali Linux**. Please keep in mind that **only 
the latest version is supported** and issues regarding different OS or distributions may **not** be resolved.

For more information see the [documentation](https://cryton.gitlab-pages.ics.muni.cz/cryton-documentation/latest/playground/introduction/).

## Quick-start
Make sure Git, Docker, and Docker Compose plugin are installed:
- [Git](https://git-scm.com/)
- [Docker Compose](https://docs.docker.com/compose/install/)

Optionally, check out these Docker [post-installation steps](https://docs.docker.com/engine/install/linux-postinstall/).

Clone the repository:
```shell
git clone https://gitlab.ics.muni.cz/cryton/cryton-playground.git
cd cryton-playground
```

Build the infrastructure:
```shell
docker compose up -d
```

Run the example scenario:
```shell
docker compose exec cryton_cli /opt/resources/run_example_scenario.sh
```

For more information see the [documentation](https://cryton.gitlab-pages.ics.muni.cz/cryton-documentation/latest/playground/introduction/).

## Contributing
Contributions are welcome. Please **contribute to the [project mirror](https://gitlab.com/cryton-toolset/cryton-playground)** on gitlab.com.
For more information see the [contribution page](https://cryton.gitlab-pages.ics.muni.cz/cryton-documentation/latest/contribution-guide/).
