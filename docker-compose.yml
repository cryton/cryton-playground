version: '3.9'
services:
  cryton_core:
    restart: always
    image: registry.gitlab.ics.muni.cz:443/cryton/cryton-core:1
    container_name: cryton-core
    hostname: core.cryton
    ports:
      - "127.0.0.1:8000:80"
    env_file:
      - .env
    depends_on:
      cryton_pgbouncer:
        condition: service_started
      cryton_rabbit:
        condition: service_healthy
    networks:
      cryton:
        ipv4_address: 192.168.90.10

  cryton_proxy:
    restart: always
    image: registry.gitlab.ics.muni.cz:443/cryton/cryton-core:proxy-1
    container_name: cryton-proxy
    network_mode: service:cryton_core
    depends_on:
      cryton_core:
        condition: service_started

  cryton_db:
    restart: always
    image: postgres:15
    container_name: cryton-db
    hostname: db.cryton
    env_file:
      - .env
    environment:
      POSTGRES_PASSWORD: $CRYTON_CORE_DB_PASSWORD
      POSTGRES_USER: $CRYTON_CORE_DB_USERNAME
      POSTGRES_DB: $CRYTON_CORE_DB_NAME
      POSTGRES_HOST_AUTH_METHOD: md5
    volumes:
      - cryton_db_data:/var/lib/postgresql/data
    healthcheck:
      test: /usr/bin/pg_isready -U $$POSTGRES_USER
      interval: 20s
      timeout: 10s
      retries: 5
    networks:
      cryton:
        ipv4_address: 192.168.90.13

  cryton_pgbouncer:
    restart: always
    image: bitnami/pgbouncer:1.18.0
    container_name: cryton-pgbouncer
    hostname: pgbouncer.cryton
    depends_on:
      cryton_db:
        condition: service_healthy
    env_file:
      - .env
    environment:
      POSTGRESQL_HOST: cryton_db
      POSTGRESQL_USERNAME: $CRYTON_CORE_DB_USERNAME
      POSTGRESQL_DATABASE: $CRYTON_CORE_DB_NAME
      POSTGRESQL_PASSWORD: $CRYTON_CORE_DB_PASSWORD
      PGBOUNCER_DATABASE: $CRYTON_CORE_DB_NAME
      PGBOUNCER_PORT: 5432
      PGBOUNCER_MAX_CLIENT_CONN: 1000
      PGBOUNCER_MIN_POOL_SIZE: 8
      PGBOUNCER_POOL_MODE: transaction
    networks:
      cryton:
        ipv4_address: 192.168.90.14

  cryton_worker:
    restart: always
    image: registry.gitlab.ics.muni.cz:443/cryton/cryton-worker:kali-1
    container_name: cryton-worker
    hostname: worker.cryton
    env_file:
      - .env
    volumes:
      - ./resources/:/opt/resources
    networks:
      cryton:
        ipv4_address: 192.168.90.11
      public:
        ipv4_address: 192.168.91.11

  cryton_empire:
    restart: always
    image: bcsecurity/empire:v4.10.0
    container_name: cryton-empire
    env_file:
      - .env
    stdin_open: true
    command: [ "server", "--username", "$CRYTON_WORKER_EMPIRE_USERNAME", "--password", "$CRYTON_WORKER_EMPIRE_PASSWORD" ]
    network_mode: service:cryton_worker

  cryton_msf:
    restart: always
    image: registry.gitlab.ics.muni.cz:443/cryton/configurations/metasploit-framework:0
    container_name: cryton-msf
    env_file:
      - .env
    environment:
      MSF_RPC_USERNAME: $CRYTON_WORKER_MSF_RPC_USERNAME
      MSF_RPC_PASSWORD: $CRYTON_WORKER_MSF_RPC_PASSWORD
      MSF_DB_HOST: $CRYTON_CORE_DB_HOST
      MSF_DB_PORT: $CRYTON_CORE_DB_PORT
      MSF_DB_NAME: $CRYTON_CORE_DB_NAME
      MSF_DB_USERNAME: $CRYTON_CORE_DB_USERNAME
      MSF_DB_PASSWORD: $CRYTON_CORE_DB_PASSWORD
      MSF_DB_PREPARED_STATEMENTS: false
      MSF_DB_ADVISORY_LOCKS: false
    tty: true
    network_mode: service:cryton_worker
    volumes:
      - ./resources/:/opt/resources
    depends_on:
      cryton_pgbouncer:
        condition: service_started

  cryton_rabbit:
    restart: always
    image: rabbitmq:3.11-management
    container_name: cryton-rabbit
    hostname: rabbit.cryton
    env_file:
      - .env
    environment:
      RABBITMQ_DEFAULT_USER: $CRYTON_CORE_RABBIT_USERNAME
      RABBITMQ_DEFAULT_PASS: $CRYTON_CORE_RABBIT_PASSWORD
    expose:
      - "5672:5672"
    healthcheck:
      test: rabbitmqctl eval '
        { true, rabbit_app_booted_and_running } = { rabbit:is_booted(node()), rabbit_app_booted_and_running },
        { [], no_alarms } = { rabbit:alarms(), no_alarms },
        [] /= rabbit_networking:active_listeners(),
        rabbitmq_node_is_healthy.
        ' || exit 1
      interval: 20s
      timeout: 10s
      retries: 5
    networks:
      cryton:
        ipv4_address: 192.168.90.15

  cryton_cli:
    restart: always
    image: registry.gitlab.ics.muni.cz:443/cryton/cryton-cli:1
    container_name: cryton-cli
    network_mode: service:cryton_core
    env_file:
      - .env
    depends_on:
      cryton_core:
        condition: service_started
    volumes:
      - ./resources/:/opt/resources
    tty: true

  cryton_frontend:
    restart: always
    image: registry.gitlab.ics.muni.cz:443/cryton/cryton-frontend:1
    ports:
      - "127.0.0.1:8080:80"
    networks:
      cryton:
        ipv4_address: 192.168.90.12

  wordpress_db:
    image: mysql:8.0.31 # 5.7 <- alternative version
    restart: always
    environment:
      MYSQL_ROOT_PASSWORD: wordpress
      MYSQL_DATABASE: wordpress
      MYSQL_USER: wordpress
      MYSQL_PASSWORD: wordpress
    volumes:
      - wordpress_db_data:/var/lib/mysql
    networks:
      dmz:
        ipv4_address: 192.168.93.11
    healthcheck:
      test: mysql $$MYSQL_DATABASE --user=$$MYSQL_USER --password=$$MYSQL_PASSWORD --silent --execute "SELECT 1;"
      interval: 10s
      timeout: 10s
      retries: 7

  wordpress_app:
    image: wordpress:6.1.1-apache  # 4.8.3-php7.1-apache <- alternative version
    restart: always
    expose:
      - 80
    environment: &wordpress_app_env
      WORDPRESS_DB_HOST: wordpress_db
      WORDPRESS_DB_USER: wordpress
      WORDPRESS_DB_PASSWORD: wordpress
      WORDPRESS_DB_NAME: wordpress
    depends_on:
      wordpress_db:
        condition: service_healthy
    volumes:
      - wordpress_app_html:/var/www/html
    networks:
      public:
        ipv4_address: 192.168.91.10
      server_net:
        ipv4_address: 192.168.92.10
      dmz:
        ipv4_address: 192.168.93.10
      user_net:
        ipv4_address: 192.168.94.10
    healthcheck:
      test: curl localhost/wp-admin/install.php | grep WordPress
      interval: 10s
      timeout: 10s
      retries: 3

  wordpress_cli:
    image: wordpress:cli-2.7.1-php8.0 # cli-php7.1 <- alternative version
    depends_on:
      wordpress_db:
        condition: service_healthy
      wordpress_app:
        condition: service_healthy
    network_mode: service:wordpress_app
    environment:
      <<: *wordpress_app_env
    volumes:
      - wordpress_app_html:/var/www/html
    entrypoint: sh
    command: -c 'wp core install --url="http://192.168.91.10" --title="wordpress" --admin_name=wordpress --admin_password="wordpress" --admin_email=wordpress@wordpress.wordpress'

  vulnerable_ftp:
    restart: always
    image: uexpl0it/vulnerable-packages:backdoored-vsftpd-2.3.4
    volumes:
      - ./resources/vsftpd.log:/var/log/vsftpd.log
    networks:
      server_net:
        ipv4_address: 192.168.92.20

  vulnerable_db:
    restart: always
    image: postgres:10.5
    environment:
      - POSTGRES_DB=beastdb
      - POSTGRES_USER=dbuser
      - POSTGRES_PASSWORD=dbpassword
    volumes:
      - vulnerable_db_data:/var/lib/postgresql/data
      # copy the sql script to create tables
      - ./resources/create_tables.sql:/docker-entrypoint-initdb.d/create_tables.sql
      # copy the sql script to fill tables
      - ./resources/fill_tables.sql:/docker-entrypoint-initdb.d/fill_tables.sql
    networks:
      server_net:
        ipv4_address: 192.168.92.21
      user_net:
        ipv4_address: 192.168.94.21

  vulnerable_user_machine:
    restart: always
    image: vulnerable_user_machine
    build:
      target: vulnerable-user
    networks:
      user_net:
        ipv4_address: 192.168.94.20

volumes:
  cryton_db_data:
  wordpress_app_html:
  wordpress_db_data:
  vulnerable_db_data:

networks:
  cryton:
    ipam:
      config:
        - subnet: 192.168.90.0/24
  public:
    ipam:
      config:
        - subnet: 192.168.91.0/24
  server_net:
    ipam:
      config:
        - subnet: 192.168.92.0/24
  dmz:
    ipam:
      config:
        - subnet: 192.168.93.0/24
  user_net:
    ipam:
      config:
        - subnet: 192.168.94.0/24
